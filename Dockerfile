FROM python:3.10-buster

WORKDIR /app

RUN pip install pipenv
COPY src/Pipfile.lock /app
COPY src/Pipfile /app
RUN pipenv install --system --deploy --ignore-pipfile

COPY src/ /app

ENTRYPOINT ["python"]
CMD ["manage.py", "runserver", "0.0.0.0:8000"]
