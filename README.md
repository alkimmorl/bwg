# bwg


### How to setup

- Install docker and docker-compose
- Create network `docker network create bwg`
- Make shared dirs `mkdir postgres`
- In application root dir run commands:
```
 make build
 make migrate
 ```

### Stop and remove containers
```
make down
```

### Run containers
``` 
make up
```

### Restart on code change

When source code modified - just restart containers

```
make restart
```

### Run tests
```
make test
```