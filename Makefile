build:
	docker-compose up -d --build
up:
	docker-compose up -d
down:
	docker-compose down
restart:
	make down
	make up
migrations:
	docker exec -it bwg-python python manage.py makemigrations
migrate:
	docker exec -it bwg-python python manage.py migrate
shell:
	docker exec -it bwg-python python manage.py shell
test:
	docker exec -it bwg-python python manage.py test