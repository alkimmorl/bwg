from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from app.views import BankAccountList, BankAccountCreate, TransactionsList, TransactionCreate, \
    BankAccountLock

urlpatterns = [
    path(settings.ADMIN_PANEL_PATH, admin.site.urls), # Secure admin panel
    path("accounts/", BankAccountList.as_view()),
    path("transactions/", TransactionsList.as_view()),
    path("transactions/<str:account_number>/", TransactionsList.as_view()),
    path("account-create/", BankAccountCreate.as_view()),
    path("transaction-create/", TransactionCreate.as_view()),
    path("account-lock/<str:account_number>/", BankAccountLock.as_view()),
]
