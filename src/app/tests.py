from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import BankAccount, Transaction

class BankAccountTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_bank_account(self):
        response = self.client.post('/account-create/', {'account_number': '123412341234123'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BankAccount.objects.count(), 0)

        response = self.client.post('/account-create/', {'account_number': '1234123412341234'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BankAccount.objects.count(), 1)
        self.assertEqual(BankAccount.objects.get().account_number, '1234123412341234')

        response = self.client.post('/account-create/', {'account_number': '1234123412341234'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BankAccount.objects.count(), 1)
        self.assertEqual(response.json()["account_number"][0], "bank account with this account number already exists.")
    
    def test_bank_account_list(self):
        self.client.post('/account-create/', {'account_number': '1234123412341234'})
        self.client.post('/account-create/', {'account_number': '1111111111111111'})
        response = self.client.get('/accounts/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()),2)
    
    def test_bank_account_lock(self):
        a1_number = '1234123412341234'
        self.client.post('/account-create/', {'account_number': a1_number})

        response = self.client.put(f"/account-lock/{a1_number}/", {"is_locked": True})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.put(f"/account-lock/{a1_number}/", {"is_locked": False})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_transactions(self):
        a1_number = '1111111111111111'
        a2_number = '2222222222222222'
        a3_number = '3333333333333333'

        self.client.post('/account-create/', {'account_number': a1_number})
        self.client.post('/account-create/', {'account_number': a2_number})
        self.client.post('/account-create/', {'account_number': a3_number})
        
        a1 = BankAccount.objects.get(account_number=a1_number)
        a2 = BankAccount.objects.get(account_number=a2_number)
        a3 = BankAccount.objects.get(account_number=a3_number)
        # insufficient funds
        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a2_number, "amount": 100}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        a1.balance = 1000
        a1.save()

        amount = 100

        # sender and receiver cannot be the same
        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a1_number, "amount": 100}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # amount must not be zero
        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a2_number, "amount": 0}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # the recipient is locked and can not receive transactions
        self.client.put(f"/account-lock/{a2_number}/", {"is_locked": True})

        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a2_number, "amount": 100}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.client.put(f"/account-lock/{a2_number}/", {"is_locked": False})

        # sender's account is locked
        self.client.put(f"/account-lock/{a1_number}/", {"is_locked": True})

        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a2_number, "amount": 100}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.client.put(f"/account-lock/{a1_number}/", {"is_locked": False})
        
        # correct transaction
        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a2_number, "amount": 100}
        )

        # Check that the transaction was made correctly
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)

        a1_updated = BankAccount.objects.get(account_number=a1_number)
        a2_updated = BankAccount.objects.get(account_number=a2_number)

        self.assertEqual(a1_updated.balance, a1.balance - amount)
        self.assertEqual(a2_updated.balance, a2.balance + amount)

        # check transactions list
        response = self.client.get('/transactions/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

        response = self.client.post(
            '/transaction-create/', 
            {'sender': a1_number, 'receiver': a3_number, "amount": 100}
        )

        response = self.client.get('/transactions/')
        self.assertEqual(len(response.json()), 2)

        # check transactions list by user
        response = self.client.get(f'/transactions/{a1_number}/')
        self.assertEqual(len(response.json()), 2)

        response = self.client.get(f'/transactions/{a2_number}/')
        self.assertEqual(len(response.json()), 1)

        response = self.client.get(f'/transactions/{a3_number}/')
        self.assertEqual(len(response.json()), 1)




    

