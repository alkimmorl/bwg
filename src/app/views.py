from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from app.models.bank import BankAccount, Transaction
from .serializers import BankAccountSerializer, BankAccountCreateSerializer, TransactionSerializer,\
    BankAccountLockSerializer
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView
from django.db.models import Q
class BankAccountList(ListAPIView):
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer

class TransactionsList(ListAPIView):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = Transaction.objects.all()
        account_number = kwargs.get('account_number', None)

        if account_number is not None:
            queryset = queryset.filter(Q(sender__account_number=account_number) | Q(receiver__account_number=account_number))

        return Response(TransactionSerializer(queryset, many=True).data)

class BankAccountCreate(CreateAPIView):
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountCreateSerializer

class TransactionCreate(CreateAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    
class BankAccountLock(UpdateAPIView):
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountLockSerializer
    lookup_field = "account_number"