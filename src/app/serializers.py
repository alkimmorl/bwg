from rest_framework import serializers
from .models import BankAccount, Transaction
from django.db import transaction
from decimal import Decimal

class BankAccountSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        return super().update(instance, validated_data)
     
    class Meta:
        model = BankAccount
        fields = '__all__'

class BankAccountCreateSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        account_number = validated_data.get("account_number")
        try:
            int(account_number)
        except:
            raise serializers.ValidationError({"error": "The account number must be a 16 digit number."})
        
        return super().create(validated_data)
    
    class Meta:
        model = BankAccount
        fields = ["account_number"]

class BankAccountLockSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = ["is_locked"]



class TransactionSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        try:
            sender = validated_data.get("sender")
            receiver = validated_data.get("receiver")
        except:
            raise serializers.ValidationError({"error": "Invalid account number."})
        
        if sender == receiver:
                raise serializers.ValidationError({"error": "Sender and Receiver cannot be the same."})
        
        # Get transaction amount
        amount = Decimal(validated_data.get("amount"))

        if amount == 0:
            raise serializers.ValidationError({"error":"Amount must not be zero."})

        # Validate funds and account lock status
        if sender.balance < amount:
            raise serializers.ValidationError({"error": "Insufficient funds."})
        
        if sender.is_locked:
            raise serializers.ValidationError({"error":"Your account is locked."})
        if receiver.is_locked:
            raise serializers.ValidationError({"error":"The recipient is locked and can not receive transactions"})
        
        with transaction.atomic():
            sender.balance -= amount
            receiver.balance += amount

            # Create the transaction and save it to the database
            tx = Transaction.objects.create(sender=sender, receiver=receiver, amount=amount)
            sender.save(update_fields=["balance"])
            receiver.save(update_fields=["balance"])

        return tx
    class Meta:
        model = Transaction
        fields = '__all__'
