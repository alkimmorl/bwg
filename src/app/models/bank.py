from django.db import models
from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible


@deconstructible
class FixedLengthValidator(BaseValidator):
    message = 'Ensure this value has %(limit_value)d character (it has %(show_value)d).'
    code = 'length'

    def compare(self, a, b):
        return a != b

    def clean(self, x):
        return len(x)

class FixedLengthCharField(models.CharField):
    def __init__(self, *args, length, **kwargs):
        self.length = length
        kwargs['max_length'] = length
        super().__init__(*args, **kwargs)
        self.validators.insert(0, FixedLengthValidator(length))

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs['max_length']
        kwargs['length'] = self.length
        return name, path, args, kwargs
class BankAccount(models.Model):
    account_number = FixedLengthCharField(length=16, primary_key=True)
    balance = models.DecimalField(max_digits=32, decimal_places=2, default=0)
    is_locked = models.BooleanField(default=False)

class Transaction(models.Model):
    sender = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='sent_transactions')
    receiver = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='received_transactions')
    amount = models.DecimalField(max_digits=32, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
