from django.contrib import admin
from app.models.bank import BankAccount, Transaction
# Register your models here.

admin.site.register(BankAccount)
admin.site.register(Transaction)